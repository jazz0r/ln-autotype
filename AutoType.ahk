;
; AutoType Version: 1.0
; Language:       English
; Platform:       Win9x/NT
; Author:         Jeff Zemla <jzemla@atronic.com>
;
; Script Function:
;	Auto type text from a defined text file into Lotus Notes body.
;
;CHANGELOG
;	1.0
;	- Initial Release
;
;

#SingleInstance force
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
#Persistent  ; Keep the script running until the user exits it.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
Menu, Tray, NoStandard
Menu, Tray, Icon, Shell32.dll, 134, 1
Menu, Tray, Tip, AutoType
Menu, Tray, Add, E&xit, AT_exit


^Numpad1::
	IfWinActive, New Memo - Lotus Notes
	{
		IfNotExist, Response1.txt
		{
			Msgbox, 16, AutoType, Response1.txt not found.
			Return
		}
		AT_clipboard := ClipboardAll
		FileRead, AT_text, Response1.txt
		Clipboard := AT_text
		ControlFocus, NotesRichText1
		ControlGetFocus, AT_control
		If AT_control <> NotesRichText1
			ControlFocus, NotesRichText2
		ControlSend, NotesRichText1, ^v
		Sleep, 1000
		Clipboard := AT_clipboard
		AT_clipboard =
	}
Return

^Numpad2::
	IfWinActive, New Memo - Lotus Notes
	{
		IfNotExist, Response2.txt
		{
			Msgbox, 16, AutoType, Response2.txt not found.
			Return
		}
		AT_clipboard := ClipboardAll
		FileRead, AT_text, Response2.txt
		Clipboard := AT_text
		ControlFocus, NotesRichText1
		ControlGetFocus, AT_control
		If AT_control <> NotesRichText1
			ControlFocus, NotesRichText2
		ControlSend, NotesRichText1, ^v
		Sleep, 1000
		Clipboard := AT_clipboard
		AT_clipboard =
	}
Return

AT_exit:
ExitApp